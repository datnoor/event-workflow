variable "sfn_name" {}
variable "sfn_arn" {}
variable "sns_arn" {}

resource "aws_cloudwatch_metric_alarm" "throttle" {
  alarm_name          = "${var.sfn_name}-throttle"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ExecutionThrottled"
  namespace           = "AWS/States"
  period              = "300"
  statistic           = "Sum"
  threshold           = "0"

  dimensions {
    StateMachineArn = "${var.sfn_arn}"
  }

  alarm_description = "This metric monitors step function throttles"
  alarm_actions     = ["${var.sns_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "abort" {
  alarm_name          = "${var.sfn_name}-abort"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ExecutionsAborted"
  namespace           = "AWS/States"
  period              = "300"
  statistic           = "Sum"
  threshold           = "0"

  dimensions {
    StateMachineArn = "${var.sfn_arn}"
  }

  alarm_description = "This metric monitors step function aborts"
  alarm_actions     = ["${var.sns_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "fail" {
  alarm_name          = "${var.sfn_name}-fail"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ExecutionsFailed"
  namespace           = "AWS/States"
  period              = "300"
  statistic           = "Sum"
  threshold           = "0"

  dimensions {
    StateMachineArn = "${var.sfn_arn}"
  }

  alarm_description = "This metric monitors step function failures"
  alarm_actions     = ["${var.sns_arn}"]
}

resource "aws_cloudwatch_metric_alarm" "timeout" {
  alarm_name          = "${var.sfn_name}-timeout"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "ExecutionsTimedOut"
  namespace           = "AWS/States"
  period              = "300"
  statistic           = "Sum"
  threshold           = "0"

  dimensions {
    StateMachineArn = "${var.sfn_arn}"
  }

  alarm_description = "This metric monitors step function timeouts"
  alarm_actions     = ["${var.sns_arn}"]
}
