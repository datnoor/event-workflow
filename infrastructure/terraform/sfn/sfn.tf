variable "project" {}
variable "env" {}
variable "name" {}

variable "sfn_role_arn" {}

locals {
  name_full = "${var.project}-${var.env}-${var.name}"
}

resource "aws_sfn_state_machine" "main" {
  name     = "${local.name_full}"
  role_arn = "${var.sfn_role_arn}"

  # State machine definition will be updated via code, therefore not tracked
  lifecycle {
    ignore_changes = [
      "definition"
    ]
  }

  definition = <<EOF
{
  "Comment": "A Hello World example of the Amazon States Language using a Pass state",
  "StartAt": "HelloWorld",
  "States": {
    "HelloWorld": {
      "Type": "Pass",
      "Result": "Hello World!",
      "End": true
    }
  }
}
EOF
}

output sfn_arn {
  value = "${aws_sfn_state_machine.main.id}"
}

output sfn_name {
  value = "${aws_sfn_state_machine.main.name}"
}