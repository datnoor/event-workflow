variable "lambda_job_data_arn" {}

## Connect Batch events to lambda (and consequently dynamodb)
resource "aws_cloudwatch_event_target" "lambda_job_data" {
  target_id = "${local.cluster_name}-lambda_job_data"
  rule      = "${aws_cloudwatch_event_rule.lambda_job_data.name}"
  arn       = "${var.lambda_job_data_arn}"
}

resource "aws_cloudwatch_event_rule" "lambda_job_data" {
  name        = "${local.cluster_name}-lambda_job_data"
  description = "Fire lambda on Batch job completion"

  event_pattern = <<PATTERN
{
  "source": [
    "aws.batch"
  ],
  "detail-type": [
    "Batch Job State Change"
  ],
  "detail": {
    "jobQueue": [
      "${aws_batch_job_queue.high_priority.arn}",
      "${aws_batch_job_queue.low_priority.arn}"
    ],
    "status": [
      "SUCCEEDED",
      "FAILED"
    ]
  }
}
PATTERN
}

resource "aws_lambda_permission" "allow_cloudwatch" {
  statement_id   = "AllowExecutionFromCloudWatch"
  action         = "lambda:InvokeFunction"
  function_name  = "${var.lambda_job_data_arn}"
  principal      = "events.amazonaws.com"
  source_arn     = "${aws_cloudwatch_event_rule.lambda_job_data.arn}"
}
