variable "s3_main_arn" {}

data "aws_iam_policy_document" "ecs_task_role_policy_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "ecs_task_role" {
  name               = "${local.cluster_name}-ecs_task_role"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_task_role_policy_document.json}"
}

data "aws_iam_policy_document" "batch_basic_policy_document" {
  statement {
    sid = "BatchS3MainBucketPolicy"

    effect = "Allow"

    actions = [
      "s3:ListBucket"
    ]

    resources = [
      "${var.s3_main_arn}",
    ]
  }

  statement {
    sid = "BatchS3MainObjectPolicy"

    effect = "Allow"

    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:DeleteObject"
    ]

    resources = [
      "${var.s3_main_arn}/*",
    ]
  }

  statement {
    sid = "BatchLambdaPolicy"

    effect = "Allow"

    actions = [
      "lambda:InvokeFunction"
    ]

    resources = [
      "arn:aws:lambda:*:*:function:${var.project}-${var.env}-*"
    ]
  }

  statement {
    sid = "BatchBatchPolicy"

    effect = "Allow"

    actions = [
      # Enable job to get desrciption of itself
      "batch:DescribeJobs",
      # Enable job to terminate itself
      "batch:CancelJob",
      "batch:TerminateJob"
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "batch_basic_policy" {
  name   = "${local.cluster_name}-batch_basic_policy"
  policy = "${data.aws_iam_policy_document.batch_basic_policy_document.json}"
}

resource "aws_iam_role_policy_attachment" "ecs_task_role_3" {
  role       = "${aws_iam_role.ecs_task_role.name}"
  policy_arn = "${aws_iam_policy.batch_basic_policy.arn}"
}

data "aws_iam_policy_document" "ecs_instance_role_policy_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "ecs_instance_role" {
  name               = "${local.cluster_name}-ecs_instance_role"
  assume_role_policy = "${data.aws_iam_policy_document.ecs_instance_role_policy_document.json}"
}

resource "aws_iam_role_policy_attachment" "ecs_instance_role_3" {
  role       = "${aws_iam_role.ecs_instance_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role"
}

resource "aws_iam_instance_profile" "ecs_instance_profile" {
  name = "${local.cluster_name}-ecs_instance_profile"
  role = "${aws_iam_role.ecs_instance_role.name}"
}

data "aws_iam_policy_document" "spot_fleet_role_policy_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["spotfleet.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "spot_fleet_role" {
  name               = "${local.cluster_name}-spot_fleet_role"
  assume_role_policy = "${data.aws_iam_policy_document.spot_fleet_role_policy_document.json}"
}

resource "aws_iam_role_policy_attachment" "spot_fleet_role_1" {
  role       = "${aws_iam_role.spot_fleet_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2SpotFleetTaggingRole"
}

data "aws_iam_policy_document" "aws_batch_service_role_policy_document" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["batch.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_role" "aws_batch_service_role" {
  name               = "${local.cluster_name}-aws_batch_service_role"
  assume_role_policy = "${data.aws_iam_policy_document.aws_batch_service_role_policy_document.json}"
}

resource "aws_iam_role_policy_attachment" "aws_batch_service_role_1" {
  role       = "${aws_iam_role.aws_batch_service_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBatchServiceRole"
}
