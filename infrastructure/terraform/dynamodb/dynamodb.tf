variable "project" {}
variable "env" {}
variable "table_name" {}

locals {
  read_capacity = 5
  write_capacity = 5
}

resource "aws_dynamodb_table" "table" {
  name           = "${var.project}-${var.env}-${var.table_name}"
  read_capacity  = "${local.read_capacity}"
  write_capacity = "${local.write_capacity}"
  hash_key       = "identifier"

  # Attribute names should not collide with DynamoDB reserved words (e.g., 
  # "key") or Python reserved words (e.g., "id")
  attribute {
    name = "identifier"
    type = "S"
  }

  server_side_encryption {
    enabled = true
  }

  point_in_time_recovery {
    enabled = false
  }

  tags {
    Project   = "${var.project}"
    Env       = "${var.env}"
    TableName = "${var.table_name}"
    ManagedBy = "terraform"
  }

}

output table_arn {
  value = "${aws_dynamodb_table.table.arn}"
}

output table_id {
  value = "${aws_dynamodb_table.table.id}"
}