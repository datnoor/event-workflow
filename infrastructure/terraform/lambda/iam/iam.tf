variable "project" {}
variable "env" {}
variable "name" {}

variable "sns_topic_dlq_arn" {}
variable "s3_main_arn" {}
variable "sfn_workflow_arn" {}
variable "sfn_poll_activity_arn" {}
variable "activity_run_jobs_arn" {}
variable "dynamodb_job_data_arn" {}

data "aws_iam_policy_document" "lambda_role_policy_document" {
  statement {
    effect = "Allow"

    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "lambda_role" {
  name = "${var.project}-${var.env}-lambda-${var.name}"
  path = "/${var.project}/${var.env}/lambda/${var.name}/"

  assume_role_policy = "${data.aws_iam_policy_document.lambda_role_policy_document.json}"

}

data "aws_iam_policy_document" "lambda_policy_document" {
  statement {
    sid = "LambdaSNSPolicy"

    effect = "Allow"

    actions = [
      "sns:Publish"
    ]

    resources = [
      "${var.sns_topic_dlq_arn}"
    ]
  }

  statement {
    sid = "LambdaLogPolicy"

    effect = "Allow"

    actions = [
      "logs:CreateLogGroup",
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "*"
    ]
  }


  statement {
    sid = "LambdaS3ListPolicy"

    effect = "Allow"

    actions = [
      "s3:ListBucket"
    ]

    resources = [
      "${var.s3_main_arn}"
    ]
  }

  statement {
    sid = "LambdaS3ReadPolicy"

    effect = "Allow"

    actions = [
      "s3:GetObject"
    ]

    resources = [
      "${var.s3_main_arn}/config/*",
      "${var.s3_main_arn}/input/*",
      "${var.s3_main_arn}/tmp/*"
    ]
  }

  statement {
    sid = "LambdaS3WritePolicy"

    effect = "Allow"

    actions = [
      "s3:PutObject"
    ]

    resources = [
      "${var.s3_main_arn}/output/*"
    ]
  }

  statement {
    sid = "LambdaEC2Policy"

    effect = "Allow"

    actions = [
      "ec2:DescribeNetworkInterfaces",
      "ec2:CreateNetworkInterface",
      "ec2:DeleteNetworkInterface"
    ]

    resources = [
      "*"
    ]
  }

  statement {
    sid = "LambdaLambdaPolicy"

    effect = "Allow"

    actions = [
      "lambda:InvokeFunction"
    ]

    resources = [
      "arn:aws:lambda:*:*:function:${var.project}-${var.env}-*"
    ]
  }

  statement {
    sid = "LambdaBatchPolicy"

    effect = "Allow"

    actions = [
      "batch:SubmitJob",
      "batch:DescribeJobs",
      "batch:CancelJob",
      "batch:TerminateJob"
    ]

    resources = [
      "*"
    ]
  }

  statement {
    sid = "LambdaSfnPolicy1"

    effect = "Allow"

    actions = [
      "states:StartExecution"
    ]

    resources = [
      "${var.sfn_workflow_arn}",
      "${var.sfn_poll_activity_arn}"
    ]
  }

  statement {
    sid = "LambdaSfnPolicy2"

    effect = "Allow"

    actions = [
      "states:DescribeExecution"
    ]

    resources = [
      "arn:aws:states:*:*:execution:${var.project}-${var.env}-workflow:*"
    ]
  }

  statement {
    sid = "LambdaActivityPolicy"

    effect = "Allow"

    actions = [
      "states:GetActivityTask",
      "states:SendTaskSuccess",
      "states:SendTaskFailure"
    ]

    resources = [
      "${var.activity_run_jobs_arn}"
    ]
  }

  statement {
    sid = "LambdaDynamo"

    effect = "Allow"

    actions = [
      "dynamodb:GetItem",
      "dynamodb:UpdateItem",
      "dynamodb:PutItem"
    ]

    resources = [
      "${var.dynamodb_job_data_arn}"
    ]
  }

}

resource "aws_iam_policy" "lambda_policy" {
  name = "${var.project}-${var.env}-lambda-${var.name}"
  path = "/${var.project}/${var.env}/lambda/${var.name}/"
  policy = "${data.aws_iam_policy_document.lambda_policy_document.json}"
}

resource "aws_iam_role_policy_attachment" "lambda_role_1" {
  role = "${aws_iam_role.lambda_role.id}"
  policy_arn = "${aws_iam_policy.lambda_policy.arn}"
}

output lambda_role_arn {
  value = "${aws_iam_role.lambda_role.arn}"
}