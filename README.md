# Scaling High-Throughput Genomics Workflows on AWS

This repository contains sample infrastructure and code for the example 
workflow outlined in our [blog post](https://medium.com/@tomaz_berisa/scaling-high-throughput-genomics-workflows-on-aws-ddca1b59b6aa).

## Dependencies

1. [Terraform](https://www.terraform.io/intro/getting-started/install.html)
2. [Python3](https://docs.python-guide.org/starting/installation/)
3. [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/installing.html)

## Create the infrastructure and deploy sample code

Make sure to have your AWS credentials properly 
[configured](https://docs.aws.amazon.com/cli/latest/userguide/cli-config-files.html).

Running the following code may incurr additional costs on your AWS account.

```
terraform init infrastructure/terraform/
terraform apply infrastructure/terraform/

./update.sh
```

## Run

Navigate to [Step Functions](https://console.aws.amazon.com/states/home) in 
the AWS console, and:

1. Start one execution of the step function ending in `-poll_activity`
2. Test multiple executions of the step function ending in `-workflow`

## Clean up

To destroy the created infrastructure, run:

```
terraform destroy infrastructure/terraform/
```
