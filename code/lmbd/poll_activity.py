import logging
import json
import os

import botocore

import common

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def poll_activity(event):
    logger.info('Event:')
    logger.info(event)
    logger.info('Configuring')
    activity_run_jobs_arn = os.environ['SFN_ACTIVITY']
    table_name = os.environ['DYNAMODB_TABLE']
    job_queue = os.environ['BATCH_QUEUE']
    job_definition = os.environ['BATCH_JOB_DEFINITION']

    # Increase read timeout for long poll as per:
    # https://docs.aws.amazon.com/step-functions/latest/apireference/API_GetActivityTask.html
    config = botocore.config.Config(read_timeout=65)
    sfn = common.thread_safe_client('stepfunctions', config=config)

    logger.info('Getting activity task')
    response = sfn.get_activity_task(activityArn=activity_run_jobs_arn)
    logger.info('response:')
    logger.info(response)

    if 'taskToken' in response:
        task_token = response['taskToken']
        logging.info(f'Found task token "{task_token}"')
        if 'input' in response:
            logger.info('Task found, proceeding')
            task_input = json.loads(response['input'])

            # Schedule job
            logger.info('Scheduling')
            batch = common.thread_safe_client('batch')
            kwargs = {
                'jobName': 'test',
                'jobQueue': job_queue,
                'jobDefinition': job_definition,
                'parameters': {
                    'task_token': task_token
                }
            }
            response = batch.submit_job(**kwargs)

            updates = {
                'jobs': [response['jobId']]
            }

            logger.info('Updating DynamoDB')
            common.dynamodb_item_update(
                table_name=table_name, 
                identifier=task_token, 
                updates=updates)

            logger.info('Scheduling complete')

        else:
            raise Exception(f'No input for task token {task_token}')

def handler(event, context):
    return poll_activity(event)
