import logging
import time
import calendar
import uuid

import botocore
import boto3.dynamodb.types

class DynamoDbUpdatesEmptyError(Exception):
    pass

class DynamoDbIllegalKey(Exception):
    pass

def thread_safe_client(service_name, *args, **kwargs):
    # Thread-safe, as per:
    # https://boto3.readthedocs.io/en/latest/guide/resources.html#multithreading-multiprocessing
    boto3_session = boto3.session.Session()
    client = boto3_session.client(service_name, *args, **kwargs)
    return client

def dynamodb_item_update(table_name, identifier, updates):
    logging.info(f'Updating DynamoDB table: {table_name}')

    # basic validation
    if not updates:
        error_msg = (
            f'No updates provided when updating {table_name}/{identifier}/'
        )
        logging.error(error_msg)
        raise DynamoDbUpdatesEmptyError(error_msg)

    # Only alphanumeric characters can be part of key for 
    # ExpressionAttributeNames and ExpressionAttributeValues
    key_translations = {key: uuid.uuid4().hex for key in updates}

    update_list = \
        [f'#{k_t} = :{k_t}' for k_t in key_translations.values()]
    update_expression = 'SET ' + ', '.join(update_list)

    expression_attribute_names = \
        {f'#{k_t}':k for k, k_t in key_translations.items()}

    srlzr = boto3.dynamodb.types.TypeSerializer()
    expression_attribute_values = \
      {f':{k_t}':srlzr.serialize(updates[k]) 
        for k, k_t in key_translations.items()}

    dynamodb = thread_safe_client('dynamodb')
    response = dynamodb.update_item(
        TableName=table_name,
        Key={
            'identifier': {
                'S': identifier
            }
        },
        UpdateExpression=update_expression,
        ExpressionAttributeNames=expression_attribute_names,
        ExpressionAttributeValues=expression_attribute_values,
        ReturnValues='ALL_NEW'
    )

    logging.info(f'Updated DynamoDB table: {table_name}')

    dsrlzr = boto3.dynamodb.types.TypeDeserializer()
    deserialized = \
        {k:dsrlzr.deserialize(v) for k, v in response['Attributes'].items()}

    return deserialized

def dynamodb_item_get(table_name, identifier, consistent_read=False):
    dynamodb = thread_safe_client('dynamodb')

    response = dynamodb.get_item(
        TableName=table_name,
        Key={
            'identifier': {
                'S': identifier
            }
        },
        ConsistentRead=consistent_read
    )
    if 'Item' in response:
        dsrlzr = boto3.dynamodb.types.TypeDeserializer()
        deserialized = \
            {k:dsrlzr.deserialize(v) for k, v in response['Item'].items()}
        return deserialized

    return None
