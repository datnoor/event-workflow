import logging
import json
import os

import botocore

import common

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def unblock_sfn(task_token, output):
    sfn = common.thread_safe_client('stepfunctions')
    # Unblock step function, tolerate multiple unblocks via try/except
    try:
        response = sfn.send_task_success(
            taskToken=task_token,
            output=output)
        return response
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == 'TaskTimedOut':
            return None
        else:
            raise

def job_data(event):
    logging.info(event)

    if not(event['detail']['status'] == 'FAILED' 
            or event['detail']['status'] == 'SUCCEEDED'):
        return

    table_name = os.environ['DYNAMODB_TABLE']
    task_token = event['detail']['parameters']['task_token']
    updates = {
        event['detail']['jobId']: {
            'status': event['detail']['status']
        }
    }

    db_item = common.dynamodb_item_update(
        table_name=table_name, 
        identifier=task_token, 
        updates=updates)

    job_ids = db_item['jobs']

    if event['detail']['status'] == 'FAILED':
        unblock_sfn(
            task_token=task_token,
            output=json.dumps('FAILED'))
    else:
        n_succeeded = 0
        for job_id in job_ids:
            if job_id in db_item:
                if db_item[job_id]['status'] == 'SUCCEEDED':
                    n_succeeded += 1

        if n_succeeded == len(job_ids):
            # Succeed only if *all* jobs succeed
            unblock_sfn(
                task_token=task_token,
                output=json.dumps('SUCCEEDED'))

def handler(event, context):
    return job_data(event)
