import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def poll_iterator(event):
  index = event['iterator']['index']
  step = event['iterator']['step']
  count = event['iterator']['count']
 
  index += step

  return {
    'index': index,
    'step': step,
    'count': count,
    'continue': index < count
  }

def handler(event, context):
    return poll_iterator(event)
